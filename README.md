# Simple Autologout

If the user has been inactive for a certain length of time,
user will be logged out and redirected to a given URL.
This is all accomplished using JS.

For a full description of the module, visit the
[project page](https://drupal.org/project/simpleautologout).

 * To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://drupal.org/project/issues/simpleautologout).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Configure permissions : 
Home >> Administration >> People
(/admin/people/permissions/module/simpleautologout)

- Configure Simple Automated logout : 
Home >> Administration >> Configuration >> People
(/admin/config/people/simple-autologout)


## Maintainers

- Abhishek Kumar - [abhishek.kumar](https://www.drupal.org/u/abhishekkumar)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
